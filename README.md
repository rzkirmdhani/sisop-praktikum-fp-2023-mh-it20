# sisop-praktikum-fp-2023-MH-IT20
Laporan pengerjaan soal Final Project Praktikum Sistem Operasi 2023 Kelompok IT20

## Anggota Kelompok:
| N0. | Nama Anggota | NRP |
|-----| ----------- | ----------- |
| 1. | M. Januar Eko Wicaksono | 5027221006 |
| 2. | Rizki Ramadhani | 5027221013 |
| 3. |Khansa Adia Rahma | 5027221071 | 

## Soal Final Project

### Sistem Database Sederhana
---

### Bagaimana Program Diakses.
---
- Program server berjalan sebagai daemon.
- Untuk bisa akses console database, perlu buka program client (kalau di Linux seperti command mysql di bash).
- Program client dan utama berinteraksi lewat socket.
- Program client bisa mengakses server dari mana saja.


### Struktur Direktori
---
```c
# Struktur repository (di repository Gitlab)
-database/
--[program_database].c
--[settingan_cron_backup]
-client/
--[program_client].c
-dump/
--[program_dump_client].c
```

- Penamaan dari program bebas (database.c)
```c
# Struktur server (ketika program dijalankan)
[folder_server_database]
-[program_database]
-database/
--[nama_database]/  → Directory
---[nama_tabel]         → File
```

```c
# Contoh struktur untuk server
database/
-program_databaseku
-databases/
--database1/
---table11
---table12
--database2/
---table21
---table22
---table23
```
- Bagaimana struktur data saat tersimpan di dalam file tabel bebas.

### Bagaimana Database Digunakan
---

A. Autentikasi
- Terdapat user dengan username dan password  yang digunakan untuk mengakses database yang merupakan haknya (database yang memiliki akses dengan username tersebut). Namun, jika user merupakan root (sudo), maka bisa mengakses semua database yang ada.
Catatan: Untuk hak tiap user tidak perlu didefinisikan secara rinci, cukup apakah bisa akses atau tidak.
```c
# Format

./[program_client_database] -u [username] -p [password]
```
```c
# Contoh

./client_databaseku -u john -p john123
```
- Username, password, dan hak akses database disimpan di suatu database juga, tapi tidak ada user yang bisa akses database tersebut kecuali mengakses menggunakan root.
- User root sudah ada dari awal.
```c
# Contoh cara user root mengakses program client

sudo ./client_database
```

- Menambahkan user (Hanya bisa dilakukan user root)
```c
# Format

CREATE USER [nama_user] IDENTIFIED BY [password_user];
```
```c
# Contoh

CREATE USER khonsu IDENTIFIED BY khonsu123;
```

B. Autorisasi
- Untuk dapat mengakses database yang dia punya, permission dilakukan dengan command. Pembuatan tabel dan semua DML butuh untuk mengakses database terlebih dahulu.
```c
# Format

USE [nama_database];
```
```c
# Contoh

USE database1;
```
- Yang bisa memberikan permission atas database untuk suatu user hanya root.
```c
# Format

GRANT PERMISSION [nama_database] INTO [nama_user];
```
```c
# Contoh

GRANT PERMISSION database1 INTO user1;
```
- User hanya bisa mengakses database di mana dia diberi permission untuk database tersebut.

C. Data Definition Language
- Input penamaan database, tabel, dan kolom hanya angka dan huruf.
- Semua user bisa membuat database, otomatis user tersebut memiliki permission untuk database tersebut.
```c
# Format

CREATE DATABASE [nama_database];
```
```c
# Contoh

CREATE DATABASE database1;
```
- Root dan user yang memiliki permission untuk suatu database untuk bisa membuat tabel untuk database tersebut, tentunya setelah mengakses database tersebut. Tipe data dari semua kolom adalah string atau integer. Jumlah kolom bebas.
```c
# Format

CREATE TABLE [nama_tabel] ([nama_kolom] [tipe_data], ...);\
```
```c
# Contoh

CREATE TABLE table1 (kolom1 string, kolom2 int, kolom3 string, kolom4 int);
```
- Bisa melakukan DROP database, table (setelah mengakses database), dan kolom. Jika sasaran drop ada maka di-drop, jika tidak ada maka biarkan.
```c
# Format

DROP [DATABASE | TABLE | COLUMN] [nama_database | nama_tabel | [nama_kolom] FROM [nama_tabel]];
```
```c
# Contoh

## Drop database

DROP DATABASE database1;

## Drop table

DROP TABLE table1;

## Drop Column

DROP COLUMN kolom1 FROM table1;
```

D. Data Manipulation Language.

- INSERT
Hanya bisa insert satu row per satu command. Insert sesuai dengan jumlah dan urutan kolom.
```c
# Format

INSERT INTO [nama_tabel] ([value], ...);
```
```c
# Contoh

INSERT INTO table1 (‘value1’, 2, ‘value3’, 4);
```

- UPDATE
Hanya bisa update satu kolom per satu command.
```c
# Format

UPDATE [nama_tabel] SET [nama_kolom]=[value];
```
```c
# Contoh

UPDATE table1 SET kolom1=’new_value1’;
```

- DELETE
Delete data yang ada di tabel.
```c
# Format

DELETE FROM [nama_tabel];
```
```c
# Contoh

DELETE FROM table1;
```

- SELECT
```c
# Format

DELETE FROM [nama_tabel];
```
```c
# Contoh 1

SELECT kolom1, kolom2 FROM table1;

# Contoh 2

SELECT * FROM table1;
```

- WHERE
Command UPDATE, SELECT, dan DELETE bisa dikombinasikan dengan WHERE. WHERE hanya untuk satu kondisi. Dan hanya ‘=’.
```c
# Format

[Command UPDATE, SELECT, DELETE] WHERE [nama_kolom]=[value];
```
```c
# Contoh

DELETE FROM table1 WHERE kolom1=’value1’;
```

E. Logging.
- Setiap command yang dipakai harus dilakukan logging ke suatu file dengan format. Jika yang eksekusi root, maka username root.
```c
# Format di dalam log

timestamp(yyyy-mm-dd hh:mm:ss):username:command
```
```c
# Contoh

2021-05-19 02:05:15:khonsu:SELECT FROM table1
```

F. Realibility
Harus membuat suatu program terpisah untuk dump database ke command-command yang akan di print ke layar. Untuk memasukkan ke file, gunakan redirection. Program ini tentunya harus melalui proses autentikasi terlebih dahulu. Ini sampai database level saja, tidak perlu sampai tabel.
```c
# Format

./[program_dump_database] -u [username] -p [password] [nama_database]
```
```c
# Contoh

./databasedump -u khonsu -p khonsu123 database1 > database1.backup
```
Contoh hasil isi file database1.backup adalah sebagai berikut.
```c
DROP TABLE table1;
CREATE TABLE table1 (kolom1 string, kolom2 int, kolom3 string, kolom4 int);

INSERT INTO table1 (‘abc’, 1, ‘bcd’, 2);
INSERT INTO table1 (‘abc’, 1, ‘bcd’, 2);
INSERT INTO table1 (‘abc’, 1, ‘bcd’, 2);
INSERT INTO table1 (‘abc’, 1, ‘bcd’, 2);

DROP TABLE table2;
CREATE TABLE table2 (kolom1 string, kolom2 int, kolom3 string, kolom4 int);

INSERT INTO table2 (‘abc’, 1, ‘bcd’, 2);
INSERT INTO table2 (‘abc’, 1, ‘bcd’, 2);
INSERT INTO table2 (‘abc’, 1, ‘bcd’, 2);
INSERT INTO table2 (‘abc’, 1, ‘bcd’, 2);
```
- Program dump database dijalankan tiap jam untuk semua database dan log, lalu di zip sesuai timestamp, lalu log dikosongkan kembali.

G. Tambahan
- Kita bisa memasukkan command lewat file dengan redirection di program client.
```c
# Contoh

./client_databaseku -u john -p john123 -d database1 < database.backup
```

H. Error Handling.
Jika ada command yang tidak sesuai penggunaannya. Maka akan mengeluarkan pesan error tanpa keluar dari program client.

### Solution
---





1. Isi dari File `playlist_keren.sh`

![Isi File 'playlist_keren.sh'](img/Soal_1/Isi_playlist_keren.sh.png)

2. Isi dari File Hasil dijalankannya `playlist_keren.sh`
![Hasil Program](img/Soal_1/Results_playlist_keren.sh.png)

### Kendala
---
1. Tidak ada kendala apapun.

### Revisi
---
> Program yang ditambahkan setelah soal shift dilakukan

1. Tidak ada, pada saat demo sudah lancar dan tidak ada tambahan dari aslab penguji


## Soal 2

### Study case soal 2
---
Shinichi merupakan seorang detektif SMA yang kembali menjadi anak kecil karena ulah organisasi hitam. Dengan tubuhnya yang mengecil, Shinichi tidak dapat menggunakan identitas lamanya sehingga harus membuat identitas baru. Selain itu, ia juga harus membuat akun baru dengan identitas nya saat ini. Bantu Shinichi membuat program register dan login agar Shinichi dapat dengan mudah membuat semua akun baru yang ia butuhkan.

### Problem
---
a. Shinichi akan melakukan register menggunakan email, username, serta password. Username yang dibuat bebas, namun email bersifat unique.

b. Shinichi khawatir suatu saat nanti Ran akan curiga padanya dan dapat mengetahui password yang ia buat. Maka dari itu, Shinichi ingin membuat password dengan tingkat keamanan yang tinggi.

- Password tersebut harus di encrypt menggunakan base64
- Password yang dibuat harus lebih dari 8 karakter
- Harus terdapat paling sedikit 1 huruf kapital dan 1 huruf kecil
- Password tidak boleh sama dengan username
- Harus terdapat paling sedikit 1 angka 
- Harus terdapat paling sedikit 1 simbol unik. 
c. Karena Shinichi akan membuat banyak akun baru, ia berniat untuk menyimpan seluruh data register yang ia lakukan ke dalam folder users file users.txt. Di dalam file tersebut, terdapat catatan seluruh email, username, dan password yang telah ia buat.

d. Shinichi juga ingin program register yang ia buat akan memunculkan respon setiap kali ia melakukan register. Respon ini akan menampilkan apakah register yang dilakukan Shinichi berhasil atau gagal

e. Setelah melakukan register, Shinichi akan langsung melakukan login untuk memastikan bahwa ia telah berhasil membuat akun baru. Login hanya perlu dilakukan menggunakan email dan password.

f. Ketika login berhasil ataupun gagal, program akan memunculkan respon di mana respon tersebut harus mengandung username dari email yang telah didaftarkan.
- Ex: 
    - LOGIN SUCCESS - Welcome, [username]
    - LOGIN FAILED - email [email] not registered, please register first

g. Shinichi juga mencatat seluruh log ke dalam folder users file auth.log, baik login ataupun register.
- Format: [date] [type] [message]
- Type: REGISTER SUCCESS, REGISTER FAILED, LOGIN SUCCESS, LOGIN FAILED
- Ex:
    - [23/09/17 13:18:02] [REGISTER SUCCESS] user [username] registered successfully
    - [23/09/17 13:22:41] [LOGIN FAILED] ERROR Failed login attempt on user with email [email]

### Solution
---
[Source Code](./soal_2/)

untuk membuat direktori baru yang akan menyimpan file playlist.csv dan file playlist_keren.sh

```c
mkdir register_login_project
cd register_login_project
```
Fungsi dari function ini adalah untuk membuat suatu directory. Sebelum menjalankan command `mkdir`, akan dicek terlebih dahulu apakah path dari directory yang akan dibuat sudah ada atau belum. Bila sudah ada, maka command `mkdir` tidak akan dijalankan.

Command `mkdir` akan membuat directory sesuai dengan path yang dioper melalui parameter `dir`. Membuat folder kerja baru

- Membuat skrip register
 
```c
touch register.sh
```
- Membuka file register.sh menggunakan nano sebagai teks editor.

```c
nano register.sh 
```

- Source Code 'register.sh'

```c
#!/bin/bash

# Meminta input dari pengguna
read -p "Masukkan email: " email
read -p "Masukkan username: " username
read -s -p "Masukkan password: " password
echo

# Memeriksa apakah email sudah terdaftar
if grep -q "$email" users.txt; then
  echo "Email sudah terdaftar. Silakan gunakan email lain."
else
  # Menyimpan informasi ke dalam file users.txt
  echo "$email:$username:$password" >> users.txt
  echo "Registrasi berhasil!"
fi

```

- Membuat file ‘user.txt’ untuk menyimpan informasi pengguna yang sudah terdaftar.

```c
touch users.txt
```

- Mengizinkan eksekusi pada skrip register.

```c
chmod +x register.sh
```

- Menjalankan registrasi

```c
./register.sh
```
- Membuka file login.sh menggunakan nano sebagai teks editor.

```c
nano login.sh
```
- Source Code 'login.sh'

```c
#!/bin/bash

# Meminta input dari pengguna
read -p "Masukkan email: " email
read -s -p "Masukkan password: " password
echo

# Cek apakah email sudah terdaftar
if grep -q "$email" users.txt; then
  # Ambil password terenkripsi dari file
  encrypted_password=$(grep "$email" users.txt | awk '{print $3}')
  
  # Dekripsi password dan cocokkan
  decrypted_password=$(echo -n "$encrypted_password" | base64 -d)
  if [ "$decrypted_password" = "$password" ]; then
    username=$(grep "$email" users.txt | awk '{print $2}')
    echo "LOGIN SUCCESS - Welcome, $username"
    log="[LOGIN] [$(date +'%d/%m/%y %H:%M:%S')] [LOGIN SUCCESS] user $username logged in successfully"
  else
    echo "LOGIN FAILED - Password salah."
    log="[LOGIN] [$(date +'%d/%m/%y %H:%M:%S')] [LOGIN FAILED] ERROR Failed login attempt on user with email $email"
  fi
else
  echo "LOGIN FAILED - email $email not registered, please register first"
  log="[LOGIN] [$(date +'%d/%m/%y %H:%M:%S')] [LOGIN FAILED] ERROR Failed login attempt on user with email $email"
fi

# Catat log
echo "$log" >> auth.log

```
- Membuat file user.txt dan auth.log kosong.

```c
touch users.txt
touch auth.log
```
- Memberikan izin eksekusi ke kedua skrip

```c
chmod +x register.sh
chmod +x login.sh
```

- Untuk registrasi

```c
./register.sh
```
- Untuk Login
```c
./login.sh
```

### Hasil
---
1. Isi dari File register.sh

![Isi register](img/Soal_2/register.sh.jpeg)

2. Isi dari File login.sh 

![Isi Login](img/Soal_2/login.sh.jpeg)


### Kendala
---
1. Error tidak bisa login
![Tidak bisa login](img/Soal_2/error_tidak_bisa_login.jpeg)

### Revisi
---
> Program yang ditambahkan setelah soal shift dilakukan

1. Tambahan untuk dibenerin lagi dibagian loginnya karena eror waktu demo 

## Soal 3

### Study case soal 3
---
Aether adalah seorang gamer yang sangat menyukai bermain game Genshin Impact. Karena hobinya, dia ingin mengoleksi foto-foto karakter Genshin Impact. Suatu saat Pierro memberikannya sebuah Tautan yang berisi koleksi kumpulan foto karakter dan sebuah clue yang mengarah ke endgame. Ternyata setiap nama file telah dienkripsi dengan menggunakan base64. Karena penasaran dengan apa yang dikatakan piero, Aether tidak menyerah dan mencoba untuk mengembalikan nama file tersebut kembali seperti semula.

### Problem
---
a. Aether membuat script bernama genshin.sh, untuk melakukan unzip terhadap file yang telah diunduh dan decode setiap nama file yang terenkripsi dengan base64 . Karena pada file list_character.csv terdapat data lengkap karakter, Aether ingin merename setiap file berdasarkan file tersebut. Agar semakin rapi, Aether mengumpulkan setiap file ke dalam folder berdasarkan region tiap karakter
- Format: Nama - Region - Elemen - Senjata.jpg

b. Karena tidak mengetahui jumlah pengguna dari tiap senjata yang ada di folder "genshin_character".Aether berniat untuk menghitung serta menampilkan jumlah pengguna untuk setiap senjata yang ada
- Format: [Nama Senjata] : [total]

Untuk menghemat penyimpanan. Aether menghapus file - file yang tidak ia gunakan, yaitu genshin_character.zip, list_character.csv, dan genshin.zip

c. Namun sampai titik ini Aether masih belum menemukan clue endgame yang disinggung oleh Pierro. Dia berfikir keras untuk menemukan pesan tersembunyi tersebut. Aether membuat script baru bernama find_me.sh untuk melakukan pengecekan terhadap setiap file tiap 1 detik. Pengecekan dilakukan dengan cara meng-ekstrak tiap gambar dengan menggunakan command steghide. Dalam setiap gambar tersebut, terdapat sebuah file txt yang berisi string. Aether kemudian mulai melakukan dekripsi dengan base64 pada tiap file txt dan mendapatkan sebuah url. Setelah mendapatkan url yang ia cari, Aether akan langsung menghentikan program find_me.sh serta mendownload file berdasarkan url yang didapatkan.

d. Dalam prosesnya, setiap kali Aether melakukan ekstraksi dan ternyata hasil ekstraksi bukan yang ia inginkan, maka ia akan langsung menghapus file txt tersebut. Namun, jika itu merupakan file txt yang dicari, maka ia akan menyimpan hasil dekripsi-nya bukan hasil ekstraksi. Selain itu juga, Aether melakukan pencatatan log pada file image.log untuk setiap pengecekan gambar
- Format: [date] [type] [image_path]
- Ex: 
    - [23/09/11 17:57:51] [NOT FOUND] [image_path]
    - [23/09/11 17:57:52] [FOUND] [image_path]

e. Hasil akhir:
= genshin_character
- find_me.sh
- genshin.sh
- image.log
- [filename].txt
- [image].jpg

### Solution 
---
[Source Code](./soal_3/)

- Pertama-tama kita akan mendownload file gdrive yang diperintahkan dengan cara:

```c
wget --no-check-certificate "https://drive.google.com/uc?id=1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2" -O "genshin.zip"

```

- Kemudian kita buat file bash 'genshin.sh' untuk dimasukkan source code seperti dibawah ini untuk meng-solve pertanyaan a dan b:

```c
genshin_zip="genshin"
genshin_character_zip="genshin_character.zip"
list_character="list_character.csv"

unzip "$genshin_zip"

  if [ $? -eq 0 ]; then
        echo "Unzip '$genshin_zip' berhasil."
  else
        echo "Error: Gagal unzip '$genshin_zip'."
        exit 1
  fi

  if [ ! -f "$genshin_character_zip" ]; then
         echo "Error: File '$genshin_character_zip' tidak ada."
        exit 1
  fi

unzip "$genshin_character_zip"

  if [ $? -eq 0 ]; then
         echo "Unzip '$genshin_character_zip' berhasil."
  else
         echo "Error: Gagal unzip '$genshin_character_zip'."
        exit 1
  fi

mkdir -p genshin_character

for encoded_filename in *.jpg; do
    decoded_filename=$(echo "$encoded_filename" | base64 -d 2>/dev/null)
    cocoklogi=$(grep -F "$decoded_filename" "$list_character")

    if [ -z "$cocoklogi" ]; then
        echo "Error: Tidak ada baris yang cocok untuk '$decoded_filename' dalam '$list_character'."
        exit 1
    fi

    nama=$(echo "$cocoklogi" | awk -F',' '{gsub("\r",""); print $1}')
    region=$(echo "$cocoklogi" | awk -F',' '{gsub("\r",""); print $2}')
    elemen=$(echo "$cocoklogi" | awk -F',' '{gsub("\r",""); print $3}')
    senjata=$(echo "$cocoklogi" | awk -F',' '{gsub("\r",""); print $4}')

    mkdir -p "genshin_character/$region"

    namacharacter="${nama}-${region}-${elemen}-${senjata}"
    mv "$encoded_filename" "genshin_character/$region/${namacharacter}.jpg"
    echo "Merename: $encoded_filename -> genshin_character/$region/${namacharacter}.jpg"
for weapon in Catalyst Sword Claymore Bow Polearm; do
        count=$(find genshin_character/ -name "*$weapon.jpg" | wc -l)
        echo "nama senjata $weapon : $count"
done

rm genshin_character.zip
rm genshin.zip
rm list_character.csv
```

- Setelah itu save dan jalankan 'genshin.sh' dengan command dibawah ini:


```c
chmod +x genshin.sh
./genshin.sh

```

- Kemudian buat file bash 'find_me.sh' untuk mencari url dan gambar yang tersembunyi. dengan cara:

```c
nano find_me.sh

```
- Isi source code dari 'find_me.sh adalah:

```c
!/bin/bash
set -x

regions=("Mondstat" "Liyue" "Inazuma" "Sumeru" "Fontaine")
status=false

for region in "${regions[@]}"; do
  cd "genshin/genshin_character/$region"
  for file in *; do
    time=$(date +"%Y/%m/%d %H:%M:%S")
    image_path="./$file"
    if [ -f "$file" ]; then
      steghide extract -sf "$file" -p ""
      name=$(awk -F' -' '{print $1}' <<< "$file")
      if [ -f "$name.txt" ]; then
        cat "$name.txt" | base64 -d > "./retracted.txt"
        status=true
      fi
    fi

    url_valid='https?://\S+'
    if [[ $status = true && $url =~ $url_valid ]]; then
      wget "$url"
      echo "[$time] [true] [$image_path]" >> "./image.log"
      break
    else
      echo "[$time] [false] [$image_path]" >> "./image.log"
    fi

    rm -f "$name.txt"
    sleep 1ue && $url =
  done
  if [ $status = true ]; then
    break
  fi
  cd - > /dev/null
done

```

- - Setelah itu save dan jalankan 'find_me.sh' dengan command dibawah ini:


```c
for region in genshin_character/*; do
    if [ -d "$region" ]; then
        for image in "$region"/*; do
            image_filename=$(echo "$image" | awk -F ' -' '{print $1}')
            image_basename=$(echo "$image_filename" | awk -F '/' '{print $3}')
            steghide extract -sf "$image" -p "" -xf "${image_basename}.txt"
            decoded_message=$(cat "${image_basename}.txt" | base64 --decode)
            timestamp=$(date '+%d/%m/%y %H:%M:%S')

if [[ "$decoded_message" == http ]]; then
    wget "$decoded_message"
    echo "[$timestamp] [FOUND] [$image]" >> image.log
    pkill -f "find_me.sh"
else
    echo "[$timestamp] [NOT FOUND] [$image]" >> image.log
    rm "${image_basename}.txt"
    fi
        sleep 1
done
    fi
done

```

- Setelah itu save dan jalankan 'find_me.sh' dengan command dibawah ini:

```c
chmod +x find_me.sh
./find_me.sh

```
maka akan mendeteksi satu-persatu dari file .jpg di dalam folder region tersebut untuk menemukan file yang tersembunyi.

- Setelah selesai dideteksi maka akan muncul file 'image.log' dan dibuka untuk melihat file tersebut sudah ditemukan apa belum. Dengan cara:


```c
cat image.log

```
Setelah ditemukan di dalam file 'image.log' ter-solve masalah file yang tersembunyi tersebut.


### Hasil
---

1. Results dari senjata

![Results Senjata](img/Soal_3/Result_Senjata.png)

2. Results Found File tersembunyi

![Results Found](img/Soal_3/File_Found.png)

### Kendala
---
1. Eror Folder tidak ditemukan

![Folder Tidak ditermukan](img/Soal_3/Error_folder_tidak_ditemukan.png)

2. Error File Not Found
![File Tidak ditermukan](img/Soal_3/Error_tidak_bisa_menemukan_file.png)


### Revisi
---
> Program yang ditambahkan setelah soal shift dilakukan

1. Diberitahu code untuk mencari url dan file yang tersembunyi, yang sebelumnya tidak bisa, setelah revisi menjadi ditemukan.


## Soal 4

### Study case soal 4
---
Defatra sangat tergila-gila dengan laptop legion nya. Suatu hari, laptop legion nya mendadak rusak 🙁 Tentu saja, Defatra adalah programer yang harus 24/7 ngoding tanpa menggunakan vscode di laptop legion nya.  Akhirnya, dia membawa laptop legion nya ke tukang servis untuk diperbaiki. Setelah selesai diperbaiki, ternyata biaya perbaikan sangat mahal sehingga dia harus menggunakan uang hasil slot nya untuk membayarnya. Menurut Kang Servis, masalahnya adalah pada laptop Defatra yang overload sehingga mengakibatkan crash pada laptop legion nya. Untuk mencegah masalah serupa di masa depan, Defatra meminta kamu untuk membuat program monitoring resource yang tersedia pada komputer.
Buatlah program monitoring resource pada laptop kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/

### Problem
---
a. Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2023-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20230131150000.log.

b. Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit. 

c. Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2023013115.log dengan format metrics_agg_{YmdH}.log 

d. Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.  

- Note:
    - Nama file untuk script per menit adalah minute_log.sh
    - Nama file untuk script agregasi per jam adalah aggregate_minutes_to_hourly_log.sh
    - Semua file log terletak di /home/{user}/log
    - Semua konfigurasi cron dapat ditaruh di file skrip .sh nya masing-masing dalam bentuk comment

- Berikut adalah contoh isi dari file metrics yang dijalankan tiap menit:

```c
mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size 15949,10067,308,588,5573,4974,2047,43,2004,/home/youruser/test/,74M
```

- Berikut adalah contoh isi dari file aggregasi yang dijalankan tiap jam:


```c
type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size minimum,15949,10067,223,588,5339,4626,2047,43,1995,/home/user/test/,50M maximum,15949,10387,308,622,5573,4974,2047,52,2004,/home/user/test/,74M average,15949,10227,265.5,605,5456,4800,2047,47.5,1999.5,/home/user/test/,62M
```


### Solution
---
[Source Code](./soal_4)

untuk membuat direktori baru yang akan menyimpan file 'minute_log.sh' dan 'aggregate_minutes_to_hourly_log' 

```c
mkdir nomor4
```
Fungsi dari function ini adalah untuk membuat suatu directory. Sebelum menjalankan command `mkdir`, akan dicek terlebih dahulu apakah path dari directory yang akan dibuat sudah ada atau belum. Bila sudah ada, maka command `mkdir` tidak akan dijalankan.

Command `mkdir` akan membuat directory sesuai dengan path yang dioper melalui parameter `dir`.

- Pertama-tama buatlah file 'minute_log.sh' dengan cara:
```c
nano minute_log.sh
```

- Source Code nano minute_log.sh
```c
#!/bin/bash

# Log directori
log_dir="/home/yanuar/nomor4"

# Pengaturan waktu yang digunakan
timestamp=$(date +'%Y%m%d%H%M%S')

# Deklarasi Ram dan swap dengan 'free -m'
ram_info=$(free -m | awk 'NR==2{print $2","$3","$4","$5","$6","$7}')
swap_info=$(free -m | awk 'NR==3{print $2","$3","$4}')

# Target path 'du -sh'
target_path="/home/yanuar/"
dir_size_info=$(du -sh "$target_path" | awk '{print $1}')

# Menyimpan Source monitoring
echo "$ram_info,$swap_info,$target_path,$dir_size_info" >> "$log_dir/metrics_$timestamp.log"
```
- Setelah itu set up dulu untuk mengatur waktunya dengan cara:
```c
crontab -e
```
- Isi crontab -e
```c
# Menjalankan skrip per menit setiap menit
* * * * * /home/imamjk/SiSop/minute_log.sh

# Menjalankan skrip agregasi per jam setiap jam
0 * * * * /home/imamjk/SiSop/aggregate_minutes_to_hourly_log.sh 
```

- Kemudian jalankan 'find_me.sh' dengan cara:
```c
chmod +x find_me.sh
./find_me.sh
```
maka akan berjalan dan menghasilkan metrics_(sesuai dengan jam yang dijalankan pada saat itu).

- kemudian untuk membuat aggregasi file dalam satuan jam dilakukan dengan membuat file bash 'aggregate_minutes_to_hourly_log' dengan cara:
```c
nano aggregate_minutes_to_hourly_log.sh
```

- Source Code nano aggregate_minutes_to_hourly_log.sh
```c
#!/bin/bash

# Direktori log
log_dir="/home/yanuar/nomor4"

# Waktu saat ini untuk mengganti agregasi per jam
hourly_timestamp=$(date +'%Y%m%d%H')

# Menggabungkan semua file log per menit dalam satu jam
cat "$log_dir/metrics_$hourly_timestamp"* > "$log_dir/metrics_agg_$hourly_timestamp.log"

# Menghitung nilai minimum, maksimum, dan rata-rata dari metrik RAM dan Disk
min_ram=$(awk -F',' 'NR>1{print $2}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -n | head -n 1)
max_ram=$(awk -F',' 'NR>1{print $2}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -n | tail -n 1)
avg_ram=$(awk -F',' 'NR>1{sum+=$2}END{print sum/(NR-1)}' "$log_dir/metrics_agg_$hourly_timestamp.log")

min_disk=$(awk -F',' 'NR>1{print $12}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -h | head -n 1)
max_disk=$(awk -F',' 'NR>1{print $12}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -h | tail -n 1)
avg_disk=$(awk -F',' 'NR>1{sum+=$12}END{print sum/(NR-1)}' "$log_dir/metrics_agg_$hourly_timestamp.log")

# Simpan hasil agregasi dalam satu file
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_cache,mem_available,swap_total,swap_used,swap_free,swap_cache,path,path_size" > "$log_dir/metrics_agg_$hourly_timestamp.log"
echo "minimum,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_disk" >> "$log_dir/metrics_agg_$hourly_timestamp.log"
echo "maximum,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_disk" >> "$log_dir/metrics_agg_$hourly_timestamp.log"
echo "average,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_disk" >> "$log_dir/metrics_agg_$hourly_timestamp.log"
```

- Kemudian jalankan 'aggregate_minutes_to_hourly_log.sh' dengan cara:
```c
chmod +x aggregate_minutes_to_hourly_log.sh
./aggregate_minutes_to_hourly_log.sh
```
maka akan mengganti file namanya mejadi metrics_agg_(sesuai jam yang dilakukan pada saat run).

- Kemudian Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file. Dengan cara:

```c
chmod 600 metrics_*.aggregate_minutes_to_hourly_log,sh*.log 
```
maka file log tersebut hanya bisa dibaca oleh user pemilik file.

### Hasil
---

1. Results metrics keduanya
![Results Metric keduanya](img/Soal_4/Results_metrcis.png)

### Kendala
---
1. Error ada newline di line 13.
![Error Line 13](img/Soal_4/Error_agg.png)

### Revisi
---
> Program yang ditambahkan setelah soal shift dilakukan

1. Pada saat demo terdapat kendala pada saat menjal
aggregate_minutes_to_hourly_log.sh jadi harus dibenerin lagi dan udah bisa


