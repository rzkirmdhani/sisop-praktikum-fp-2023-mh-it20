#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define BUFFER_SIZE 1024

#define ADDRESS "127.0.0.1"
#define PORT 8080

char* create_formatted_string(const char* format, ...) {
    va_list args;
    va_start(args, format);

    size_t length = vsnprintf(NULL, 0, format, args) + 1;
    va_end(args);

    va_start(args, format);
    char* buffer = (char*) malloc(length * sizeof(char));
    vsnprintf(buffer, length, format, args);
    va_end(args);
    
    return buffer;
}

int main(int argc, char const *argv[])
{
    int sock = 0, valread;
    struct sockaddr_in address;
    struct sockaddr_in serv_addr;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, ADDRESS, &serv_addr.sin_addr) <= 0)
    {
        printf("\nInvalid address or address not supported \n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\nConnection Failed \n");
        return -1;
    }

    char user_name[BUFFER_SIZE];
    char user_password[BUFFER_SIZE];
    char database_name[BUFFER_SIZE];
    char database1[BUFFER_SIZE];

    if (geteuid() == 0)
        strcpy(user_name, "root");
    else if (
        argc != 5 && argc != 7 || (argc == 5 &&
                                   strcmp(argv[1], "-u") != 0 &&
                                   strcmp(argv[3], "-p") != 0))
    {
        printf(
            "%s\n%s %s\n",
            "\e[91mError\e[39m: Invalid command",
            argv[0],
            "-u \e[3muser_name\e[0m -p \e[3muser_password\e[0m");
        return EXIT_FAILURE;
    }
    else if (argc == 7 && argc == 7 &&
             strcmp(argv[1], "-u") == 0 &&
             strcmp(argv[3], "-p") == 0 &&
             strcmp(argv[5], "-d") == 0)
    {
        strcpy(user_name, argv[2]);
        strcpy(user_password, argv[4]);
        strcpy(database_name, argv[6]);
        printf("%s\n", database_name);

        char *request, response[BUFFER_SIZE];
        request = format_string("%s;\nAUTH USER %s IDENTIFIED BY %s USING %s;", user_name, user_name, user_password, database_name);
        send(sock, request, BUFFER_SIZE, 0);

        recv(sock, response, BUFFER_SIZE, 0);
        request = format_string("%s;\nUSE %s", user_name, database_name);
        printf("%s\n", request);
        send(sock, request, BUFFER_SIZE, 0);

        recv(sock, response, BUFFER_SIZE, 0);

        while (fgets(database1, BUFFER_SIZE, stdin) != NULL)
        {
            printf("%s", database1);
            request = format_string("%s;\n%s", user_name, database1);
            send(sock, request, BUFFER_SIZE, 0);

            recv(sock, response, BUFFER_SIZE, 0);
        }
        if (!strstr(response, "OK"))
        {
            printf("%s\n", response);
            return EXIT_FAILURE;
        }

        free(request);
        return 0;
    }
    else
    {
        strcpy(user_name, argv[2]);
        strcpy(user_password, argv[4]);

        char *request, response[BUFFER_SIZE];
        request = format_string("%s;\nAUTH USER %s IDENTIFIED BY %s;", user_name, user_name, user_password);
        send(sock, request, BUFFER_SIZE, 0);

        recv(sock, response, BUFFER_SIZE, 0);
        if (!strstr(response, "OK"))
        {
            printf("%s\n", response);
            return EXIT_FAILURE;
        }
        free(request);
    }

    while (1)
    {
        char command[BUFFER_SIZE];
        char *request, response[BUFFER_SIZE];

        scanf(" %[^\n]", command);
        request = format_string("%s;\n%s", user_name, command);
        send(sock, request, BUFFER_SIZE, 0);

        recv(sock, response, BUFFER_SIZE, 0);
        printf("%s\n", response);

        free(request);
    }

    return 0;
}
