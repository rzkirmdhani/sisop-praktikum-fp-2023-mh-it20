#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define BUFFER_SIZE 1024

#define ADDRESS "127.0.0.1"
#define PORT 8080

int main(int argc, char const *argv[])
{
    int sock = 0, valread;
    struct sockaddr_in address;
    struct sockaddr_in serv_addr;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Terjadi kesalahan dalam pembuatan socket \n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, ADDRESS, &serv_addr.sin_addr) <= 0)
    {
        printf("\n Alamat tidak valid atau tidak didukung \n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\n Koneksi gagal \n");
        return -1;
    }

    char user_name[BUFFER_SIZE];
    char user_password[BUFFER_SIZE];
    char database_name[BUFFER_SIZE];

    if (geteuid() == 0)
        strcpy(user_name, "root");
    else if (argc != 6 || (argc == 6 &&
                           strcmp(argv[1], "-u") != 0 &&
                           strcmp(argv[3], "-p") != 0))
    {
        printf(
            "%s\n%s %s\n",
            "\e[91mError\e[39m: Perintah tidak valid",
            argv[0],
            "-u \e[3muser_name\e[0m -p \e[3muser_password\e[0m \e[3mdatabase\e[0m");
        return EXIT_FAILURE;
    }
    else
    {
        strcpy(user_name, argv[2]);
        strcpy(user_password, argv[4]);
        strcpy(database_name, argv[5]);

        char request[BUFFER_SIZE], response[BUFFER_SIZE];
        sprintf(request, "%s;\nBACKUP %s AUTH USER %s IDENTIFIED BY %s;", user_name, database_name, user_name, user_password);
        send(sock, request, BUFFER_SIZE, 0);

        recv(sock, response, BUFFER_SIZE, 0);
        if (!strstr(response, "OK"))
        {
            printf("%s\n", response);
            return EXIT_FAILURE;
        }
    }

    return 0;
}
